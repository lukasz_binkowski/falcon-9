﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameState : MonoBehaviour {
    public static GameState instance;
    public Text scoreText;
    private int winScore;
    private int totalScore;
	public Text fuelText;
	public Text monoPropellantText;
	public Text yText;
	public Text xText;

    void Awake()
    {
        instance = this;
    }
    void Start () {
        LoadPlayerProgress();
        UpdateScoreText();
    }

	void Update(){
		updateVelocityText();
		updateFuelText();
		updateMonoPropellantText();
	}
	void updateFuelText()
	{
		fuelText.text = "Fuel: " + (GameControl.instance.rocketScript.fuel > 0 ? GameControl.instance.rocketScript.fuel : 0).ToString("0.00");
	}

	void updateVelocityText()
	{
		yText.text = GameControl.instance.rocketScript.y.ToString("0.00") + " y";
		xText.text = GameControl.instance.rocketScript.x.ToString("0.00") + " x";
	}

	void updateMonoPropellantText()
	{
		monoPropellantText.text = "Mono Propellant: " + (GameControl.instance.rocketScript.monoPropellant > 0 ? GameControl.instance.rocketScript.monoPropellant : 0).ToString("0.00");
	}
    public void win()
    {
        winScore++;
        totalScore++;
		UpdateScoreText();
    }

    public void lose()
    {
        totalScore++;
		UpdateScoreText();
    }

    void UpdateScoreText () {
        scoreText.text = winScore + "/" + totalScore;
		SavePlayerProgress ();
    }
		
    private void SavePlayerProgress()
    {
        PlayerPrefs.SetInt("SuccessfulCount", winScore);
        PlayerPrefs.SetInt("TotalCount", totalScore);
    }

    private void LoadPlayerProgress()
    {
        if (PlayerPrefs.HasKey("SuccessfulCount"))
        {
			winScore = PlayerPrefs.GetInt("SuccessfulCount");
        }
        else {
            winScore = 0;
        }
        if (PlayerPrefs.HasKey("TotalCount"))
        {
			totalScore = PlayerPrefs.GetInt("TotalCount");
        }
        else
        {
            totalScore = 0;
        }
    }
}
