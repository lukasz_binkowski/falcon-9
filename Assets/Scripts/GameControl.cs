﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {
    public static GameControl instance;
	[HideInInspector]
    public Rocket rocketScript;
    public GameObject rocketPrefab;
    public Transform placeToRespown;
    public float seaLevel = -3.6f;
    private GameObject rocket;
	public bool gameFinished;
	[HideInInspector]
	public bool moveUp;
	[HideInInspector]
	public bool moveLeft;
	[HideInInspector]
	public bool moveRight;
    void Awake () {
        instance = this;
    }
	void Start()
	{
		CreateRocket();

	}
    void CreateRocket()
    {
        rocket = Instantiate(rocketPrefab, placeToRespown.position, Quaternion.identity) as GameObject;
        rocketScript = rocket.GetComponent<Rocket>();
		gameFinished = false;
    }

    public void EndGame(bool successful) {
		if (!gameFinished) {
			gameFinished = true;
			if (successful) {
				Debug.Log ("Win");
				GameState.instance.win ();
				Destroy (rocket, 2);
				Invoke ("CreateRocket", 2.1f);
			} else {
				GameState.instance.lose ();
				Instantiate (rocketScript.explosion, rocket.transform.position, Quaternion.identity);
				Destroy (rocket);
				CreateRocket ();
			}
		}
	}


	void Update () {
        if (rocketScript!=null)
        {
			CheckRocketHeight();
			CheckWinPosition ();
        }
       
    }


    void CheckRocketHeight() {
        if (rocket.transform.position.y < seaLevel) {
            rocketScript.Destroy();
            EndGame(false);
        }
    }
	void CheckWinPosition() {
		if (rocketScript.y == 0 && rocketScript.x ==0 && rocketScript.isOnPlatform) {
			GameControl.instance.EndGame (true);
		}
	}

   
}
