﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "Rocket/Type", order = 1)]
public class RocketType : ScriptableObject
{
    public string objectName = "New Type Rocket";
    public float engineStrength;
    public float monoPropellantCapacity;
    public float propellantConsumption;
    public float fuelCapacity;
    public float fuelConsumption;
    public float mass;
    public float tiltStrength;
    public float gravity;
	public float saveMinValueY;
	public float saveMaxValueY;
	public float saveMinValueX;
	public float saveMaxValueX;
}