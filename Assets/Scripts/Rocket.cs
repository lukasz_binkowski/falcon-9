﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

enum Direction
{
	UP, LEFT, RIGHT
} 
[System.Serializable]
public class Rocket : MonoBehaviour
{
    public float y;
    public float x;
    public RocketType rocketType;
    public float monoPropellant;
	public float tiltAmount = 0f;
    public float fuel;
    public GameObject gameState;
    public GameObject explosion;

    private Rigidbody2D rb;
    private Vector2 startPosition;
    private Quaternion startRotation;
	public bool isOnPlatform = false;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        updateRigidbody();
        startPosition = transform.position;
        startRotation = transform.rotation;
		Reset();
    }

    void updateRigidbody()
    {
        rb.gravityScale = rocketType.gravity;
        rb.mass = rocketType.mass;
    }

    // Update is called once per frame
    void tilt(Direction direction)
    {
        if (monoPropellant > 0)
        {

            tiltAmount = (direction == Direction.RIGHT ? 1 : -1) * rocketType.tiltStrength / 10;
            rb.AddForceAtPosition(transform.right * tiltAmount, transform.TransformPoint(0, 2, 0));
            monoPropellant -= rocketType.propellantConsumption;
        }
    }
		
    void accelerate()
    {
        if (fuel > 0)
        {
            rb.AddRelativeForce(new Vector2(0, rocketType.engineStrength));
            fuel -= rocketType.fuelConsumption;
        }
    }
		

    public void Reset()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
        fuel = rocketType.fuelCapacity;
        monoPropellant = rocketType.monoPropellantCapacity;
		GameControl.instance.gameFinished = false;
        rb.WakeUp();
        rb.velocity = new Vector2(0.7f,-3);
        tiltAmount = 1 * rocketType.tiltStrength / 3 ;
        rb.AddForceAtPosition(transform.right * tiltAmount, transform.TransformPoint(0, 2, 0));
    }

    void processInput()
    {
        
		if (Input.GetKey(KeyCode.W) || CrossPlatformInputManager.GetButton("GoUp"))
        {
            accelerate();
        }
		if (Input.GetKey(KeyCode.A) || CrossPlatformInputManager.GetButton("GoLeft"))
        {
            tilt(Direction.LEFT);
        }
		if (Input.GetKey(KeyCode.D) || CrossPlatformInputManager.GetButton("GoRight"))
        {
            tilt(Direction.RIGHT);
        }
    }

    void Update()
    {
        y = rb.velocity.y;
        x = rb.velocity.x;

		if (!GameControl.instance.gameFinished)
        {
            processInput();
        }
    }
		

    public void Destroy()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
