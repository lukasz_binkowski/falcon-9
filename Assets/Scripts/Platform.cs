﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    Rocket rocket;

    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("OnTriggerEnter2D");
            rocket = other.GetComponent<Rocket>();
			if (rocket.y < rocket.rocketType.saveMaxValueY && rocket.x > rocket.rocketType.saveMinValueX && rocket.x < rocket.rocketType.saveMaxValueX) {
				GameControl.instance.EndGame (false);
			} else {
				rocket.isOnPlatform = true;
			}
        }
    }

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Debug.Log("OnTriggerExit2D");
			rocket = other.GetComponent<Rocket>();
			rocket.isOnPlatform = false;
		}
	}
}